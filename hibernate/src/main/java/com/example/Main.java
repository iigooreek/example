package com.example;


import com.example.data.dao.impl.EmployeeDao;
import com.example.data.entity.Employee;
import com.example.data.entity.Position;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {

    public static void main(String[] args) {

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("employee");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        EmployeeDao employeeDao = new EmployeeDao(entityManager);

        Employee employee = Employee.builder()
                .firstName("John")
                .lastName("Mayer")
                .salary(2000.0)
                .position(Position.DEVELOPER)
                .build();

        employeeDao.create(employee);
        Employee employee1 = employeeDao.get(1L);
        System.out.println(employee1);

    }
}
