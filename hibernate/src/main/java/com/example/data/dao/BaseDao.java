package com.example.data.dao;

import java.util.List;

public interface BaseDao<T> {

    T create(T t);

    void create(List<T> list);

    T get(long id);

    List<T> getAll();

    T update(T t);

    boolean delete(long id);

}
