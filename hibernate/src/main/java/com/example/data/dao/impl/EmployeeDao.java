package com.example.data.dao.impl;

import com.example.data.dao.BaseDao;
import com.example.data.entity.Employee;

import javax.persistence.EntityManager;
import java.util.List;

public class EmployeeDao implements BaseDao<Employee> {

    private final EntityManager entityManager;

    public EmployeeDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Employee create(Employee employee) {
        this.entityManager.getTransaction().begin();

        this.entityManager.persist(employee);

        this.entityManager.getTransaction().commit();

        return employee;
    }

    @Override
    public void create(List<Employee> list) {
        this.entityManager.getTransaction().begin();

        for (Employee employee : list) {
            this.entityManager.persist(employee);

            this.entityManager.flush();

            this.entityManager.clear();
        }
        this.entityManager.getTransaction().commit();
    }

    @Override
    public Employee get(long id) {
        return this.entityManager.find(Employee.class, id);
    }

    @Override
    public List<Employee> getAll() {
        return this.entityManager
                .createQuery("select e from Employee e")
                .getResultList();
    }

    @Override
    public Employee update(Employee employee) {
        return this.entityManager.merge(employee);
    }

    @Override
    public boolean delete(long id) {
        int rowCount =
                entityManager
                        .createQuery("delete from Employee e where e.ID=:id")
                        .setParameter("id", id)
                        .executeUpdate();

        return rowCount != 0;
    }
}
