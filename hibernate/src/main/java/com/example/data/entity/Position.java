package com.example.data.entity;

public enum Position {

    MANAGER,
    DEVELOPER,
    DESIGNER,
    BA

}
