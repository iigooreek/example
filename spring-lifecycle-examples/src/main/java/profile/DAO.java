package profile;

public interface DAO {
    void save(String value);
    String readAll();
}