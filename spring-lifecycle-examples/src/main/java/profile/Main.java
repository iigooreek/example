package profile;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import profile.file_database.FileDatabaseConfig;
import profile.in_memory.InMemoryDatabaseConfig;

public class Main {
    public static void main(String[] args) {
        GenericApplicationContext ctx = new AnnotationConfigApplicationContext(
                InMemoryDatabaseConfig.class,
                FileDatabaseConfig.class,
                Service.class);
        Service service = ctx.getBean("service", Service.class);
        service.save("test");
        System.out.println(service.readAll());
        ctx.close();
    }
}