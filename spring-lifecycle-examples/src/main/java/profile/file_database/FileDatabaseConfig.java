package profile.file_database;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import profile.DAO;

@Configuration
@Profile("file")
public class FileDatabaseConfig {

    @Bean
    public DAO DAO() {
        FileDatabaseDAO fileDatabaseDAO = new FileDatabaseDAO();
        fileDatabaseDAO.setFilePath(System.getProperty("java.io.tmpdir") + System.getProperty("file.separator") + "test.txt");
        return fileDatabaseDAO;
    }

}
