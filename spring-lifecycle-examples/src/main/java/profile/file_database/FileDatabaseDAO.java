package profile.file_database;

import profile.DAO;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileDatabaseDAO implements DAO {

    private File file;

    private String filePath;

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @PostConstruct
    public void init() throws Exception {
        System.out.println("Initializing Bean");
        if (filePath == null) {
            throw new IllegalArgumentException("You must specify the filePath property");
        }
        this.file = new File(filePath);
        this.file.createNewFile();
        System.out.println("File exists: " + file.exists());
    }

    @PreDestroy
    void destroy() {
        System.out.println("Destroying Bean");
        if (!file.delete()) {
            System.err.println("ERROR: failed to delete file.");
        }
        System.out.println("File exists: " + file.exists());
    }

    @Override
    public void save(String value) {
        try {
            Files.write(Paths.get(filePath).toAbsolutePath(), value.getBytes());
        } catch (IOException e) {
            System.err.println(e);
        }

    }

    @Override
    public String readAll() {
        try {
            return Files.readString(Paths.get(filePath).toAbsolutePath());
        } catch (IOException e) {
            System.err.println(e);
            return null;
        }
    }
}