package profile.in_memory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import profile.DAO;

@Configuration
@Profile("memory")
public class InMemoryDatabaseConfig {

    @Bean
    public DAO DAO() {
        return new InMemoryDatabaseDAO();
    }

}
