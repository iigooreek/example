package profile.in_memory;

import profile.DAO;

import java.util.LinkedHashSet;

public class InMemoryDatabaseDAO implements DAO {

    private LinkedHashSet<String> storage = new LinkedHashSet<>();

    @Override
    public void save(String value) {
        storage.add(value);
    }

    @Override
    public String readAll() {
        return storage.toString();
    }
}
